package Tasks;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import salesforce.LogAnalysisUtil;
import salesforce.log.GovernorLimitAnalyzer;
import salesforce.log.PipelineLineAnalyzer;
import salesforce.log.SoqlAnalyzer;
import salesforce.log.event.AggregateMap;

import java.io.*;

/**
 * Created by GTAN on 2/17/2016.
 */
public class LogAnalyzer {

    private static final String ANALYSIS_FOLDER = "ANALYSIS";
    private static LogAnalysisUtil analysisUtil;
    private static ObjectMapper objectMapper = new ObjectMapper() ;
    static {
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT) ;
    }
    public static void main(String[] args) throws Exception {
        PipelineLineAnalyzer pipelineAnalyzer = new PipelineLineAnalyzer();
        pipelineAnalyzer.addAnalyzer(new GovernorLimitAnalyzer());
        pipelineAnalyzer.addAnalyzer(new SoqlAnalyzer());

        AggregateMap m = pipelineAnalyzer.analyze(loadStreamsToProcess("logs/")) ;

        objectMapper.writeValue(System.out , m);
    }

    private static void analyze(String[] args) {
        analysisUtil = new LogAnalysisUtil(null);
        if(args.length == 1) {
            File[] rawLogFiles = loadFilesToProcess(args[0]);
            if(rawLogFiles != null){
                for (Integer x=0; x < rawLogFiles.length; x++){
                    analysisUtil.ProcessSingleLog(rawLogFiles[x]);
                }
            }
        }else {
            System.out.println("Error:Incomplete arguments.");
            System.out.println("Include filename/folder to analyse");
        }
        //analysisUtil.ProcessSingleLog(loadFilesToProcess("LOGS/07LN000000TDvf0MAD.txt")[0]);
    }

    private static File[] loadFilesToProcess(String fileOrFolderName){
        File inFile = new File(fileOrFolderName);
        File[] filesToProcess;
        try {
            if (inFile.exists()){
                if(inFile.isDirectory()){
                    return inFile.listFiles(new FilenameFilter() {
                        @Override
                        public boolean accept(File dir, String name) {
                            return  name.endsWith(".log") ;
                        }
                    });
                }else {
                    filesToProcess = new File[1];
                    filesToProcess[0] = inFile;
                }
            }else {
                System.out.println("File does not exist.");
                return null;
            }
        }catch (Exception e){
            System.out.println(e.getMessage().toString());
            return null;
        }

        return filesToProcess;
    }

    private static Reader[] loadStreamsToProcess(String fileOrFolderName) throws FileNotFoundException {
        File[] files = loadFilesToProcess(fileOrFolderName);
        Reader[] ret = new Reader[files.length];
        for(int i = 0 ; i < files.length ; i++)
            ret[i] = new FileReader(files[i]);
        return ret ;
    }


}
