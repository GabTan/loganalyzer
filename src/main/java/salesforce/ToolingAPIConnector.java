package salesforce;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import salesforce.model.ApexLog;
import salesforce.model.DebugLevel;
import salesforce.model.QueryResponse;
import salesforce.model.TraceFlag;

/**
 * Created by GTAN on 2/9/2016.
 */
public class ToolingAPIConnector {
    private static Logger logger = LoggerFactory.getLogger(ToolingAPIConnector.class);
    DateFormat DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSSZ");

    private String sessionId;
    private String serverURL;
    private static final String GET_HTTP = "GET";
    private static final String POST_HTTP = "POST";
    private ObjectMapper objectMapper;

    public ToolingAPIConnector(String sessionId, String serverURL) {
        this.sessionId = sessionId;
        this.serverURL = serverURL;
        objectMapper = new ObjectMapper();
        objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
    }

    public String queryDebugLevel(String debugLevel) throws IOException {
        String baseUrl = serverURL + "/services/data/v35.0/tooling/query?q=";
        String query = "select id from DebugLevel where DeveloperName='" + debugLevel + "'";

        //print result
        String response = sendHttpRequest(baseUrl + URLEncoder.encode(query, "UTF-8"), GET_HTTP, null);
        DebugLevel[] debugRecords = objectMapper.readValue(response, DebugLevel[].class);
        if (debugRecords.length > 0) {
            return debugRecords[0].getId();
        }
        return null;
    }

    public void createSingleTraceFlag(String userId, String debugLevelId) {
        TraceFlag flag = new TraceFlag();
        flag.setDebugLevelId(debugLevelId);
        flag.setLogType("USER_DEBUG");
        flag.setTraceEntityId(userId);

        String url = serverURL + "/services/data/v35.0/tooling/sobjects/TraceFlag/";
        try {
            sendHttpRequest(url, POST_HTTP, objectMapper.writeValueAsString(flag));
        } catch (Exception e) {
            logger.error("Error Creating Trace Flag: " + e.getMessage());
        }
    }

    public ApexLog[] queryApexLogIds(Date startTime, String[] userIds) throws IOException {
        String baseUrl = serverURL + "/services/data/v35.0/tooling/query?q=";
        String query =
                "SELECT id ,StartTime, LogUserId, Operation, Request, Status , Application , DurationMilliseconds , Location " +
                        "FROM ApexLog ";

        if (startTime != null || userIds != null)
            query += " WHERE ";
        if (startTime != null) {
            query += "  StartTime > " + DATETIME_FORMAT.format(startTime);
            if (userIds != null)
                query += " AND ";
        }
        if (userIds != null)
            query += " LogUserId IN(" + String.join(",", userIds) + ") ";

        logger.info("Getting Logs. Query:{} " , query);

        QueryResponse<ApexLog> res = objectMapper.readValue(sendHttpRequest(baseUrl + URLEncoder.encode(query, "UTF-8"), GET_HTTP, null),
                objectMapper.getTypeFactory().constructParametricType(QueryResponse.class, ApexLog.class));
        return res.getRecords() ;

    }

    public InputStream querySingleApexLog(String apexLogId) throws IOException {
        String url = serverURL + "/services/data/v28.0/tooling/sobjects/ApexLog/" + apexLogId + "/Body";
        return sendHttpRequestStream(url, GET_HTTP, null);
    }

    private String sendHttpRequest(String serverURL, String requestMethod, String jsonBody) throws IOException {
        try {

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(sendHttpRequestStream(serverURL, requestMethod, jsonBody)));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            return response.toString();

        } catch (Exception e) {
            logger.error("Cannot Send the HTTP Request:" + e.getMessage());
            throw e;
        }
    }

    private InputStream sendHttpRequestStream(String serverURL, String requestMethod, String jsonBody) throws IOException {

        URL obj = new URL(serverURL);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod(requestMethod);

        //add request header
        con.setRequestProperty("Authorization", "Bearer " + sessionId);
        con.setRequestProperty("Content-Type", "application/json");

        if (requestMethod == POST_HTTP) {
            con.setDoOutput(true);
            OutputStream os = con.getOutputStream();
            os.write(jsonBody.getBytes());
            os.flush();
        }
        logger.info(String.valueOf(con.getResponseCode()));
        logger.info(con.getResponseMessage());

        return con.getInputStream();


    }





}