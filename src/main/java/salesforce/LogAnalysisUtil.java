package salesforce;

import com.sforce.bulk.CsvWriter;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by GTAN on 2/17/2016.
 */
public class LogAnalysisUtil {
    private static final String ANALYSIS_FOLDER = "ANALYSIS";
    private static final String START_MARK = "LIMIT_USAGE_FOR_NS";
    private static final String END_MARK = "CUMULATIVE_LIMIT_USAGE_END";
    private static final String ERROR_MARK = "|FATAL_ERROR|";
    private static final String EXCEPTION_MARK = "EXCEPTION_THROWN";
    private static final String ENTITY_MARKER = "CODE_UNIT_STARTED";
    private static final String SEPARATOR = ": ";
    private static final String ENTITY_NAME = "entity";
    private static final String LOG_NAME = "Log File Name";
    private CsvWriter writer;
    private static final String NEWLINE = "\n";
    private static final String CUSTOM_RETURN = ":::";
    private HashMap<String, String> fieldMap;
    private File logsTodayFolder;
    private String[] logHeaderOrder;

    private void initFieldMap() {
        logHeaderOrder = new String[] {"Class/Trigger",
                LOG_NAME,
                "SOQL Queries",
                "SOQL Rows",
                "DML Statements",
                "DML Rows",
                "SOSL Queries",
                "Max CPU Time",
                "Max Heap Size",
                "Callouts",
                "Email Invocations",
                "Future Calls",
                "Queueable Jobs",
                "Mobile Apex",
                "Error"};
        fieldMap = new HashMap<String, String>();
        fieldMap.put("callouts","Callouts");
        fieldMap.put("mobile apex push calls","Mobile Apex");
        fieldMap.put("sosl queries","SOSL Queries");
        fieldMap.put("future calls","Future Calls");
        fieldMap.put("dml rows","DML Rows");
        fieldMap.put("email invocations","Email Invocations");
        fieldMap.put("query rows","SOQL Rows");
        fieldMap.put("dml statements","DML Statements");
        fieldMap.put("soql queries","SOQL Queries");
        fieldMap.put("maximum heap size","Max Heap Size");
        fieldMap.put("maximum cpu time","Max CPU Time");
        fieldMap.put("queueable jobs added to the queue","Queueable Jobs");
        fieldMap.put("entity","Class/Trigger");
        fieldMap.put("Error","Error");
        fieldMap.put(LOG_NAME, LOG_NAME);
    }
    public LogAnalysisUtil(String folderName){
        try {
            initFieldMap();
            File analysisFolder = new File(folderName);
            if(analysisFolder.exists()){
                if(analysisFolder.isDirectory()) {
                }else {
                    analysisFolder = analysisFolder.getParentFile();
                }
                initLogIndex(analysisFolder);
            }
        } catch (Exception e){
            System.out.println(e.getMessage().toString());
        }
    }

    public void ProcessSingleLog(File logFile){
        BufferedReader br = null;
        String fullLogContents = "";
        try {
            String sCurrentLine;
            br = new BufferedReader(new FileReader(logFile));
            while ((sCurrentLine = br.readLine()) != null) {
                fullLogContents = fullLogContents + NEWLINE + sCurrentLine;
            }
            String[] respBodyList = processWholeLogBody(fullLogContents, logFile.getName());
            for (String limitResp : respBodyList){
                createLogEntry(processLimitString(limitResp));
            }
        } catch( Exception e){
            System.out.println(e.getMessage());
            System.out.println(e.getStackTrace().toString());
        }
    }

    public HashMap<String, String> processLimitString(String limitString){
        String[] respLines = limitString.split(NEWLINE);
        HashMap<String, String> limitValMap = new HashMap<String, String>();
        String[] lineSplits = null;
        for(String it : respLines){
            // limits debug lines
            if(it.contains(" out of ")){
                lineSplits = it.trim().split(SEPARATOR);
                String[] limitVal = lineSplits[1].trim().split(" ");
                if (lineSplits[0].contains("Number of ")){
                    lineSplits[0] = lineSplits[0].substring(10).toLowerCase();
                }else {
                    lineSplits[0] = lineSplits[0].toLowerCase();
                }
                limitValMap.put(fieldMap.get(lineSplits[0]), limitVal[0]);
            }else if(it.contains(ERROR_MARK)){
                limitValMap.put("Error", it.substring(it.lastIndexOf(SEPARATOR) + 1));
            }else if(it.contains(ENTITY_NAME)){
                lineSplits = it.trim().split(SEPARATOR);
                limitValMap.put(fieldMap.get(ENTITY_NAME), lineSplits[1]);
            } else if (it.contains(LOG_NAME)){
                lineSplits = it.trim().split(SEPARATOR);
                limitValMap.put(LOG_NAME, lineSplits[1]);
            }
        }

        if(!limitValMap.isEmpty()){
            return limitValMap;
        }
       return null;
    }

    public void createLogEntry(HashMap<String, String> valueMap){
        try {
            String[] logEntry = new String[logHeaderOrder.length];
            HashMap<String, String> logItem = new HashMap<String, String>();
            Integer x = 0;
            for (String fieldName : logHeaderOrder) {
                logEntry[x] = valueMap.get(fieldName);
                x++;
            }
            saveToFile(logEntry);
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    private void saveToFile(String[] singleLogEntry) throws Exception{
            writer.writeRecord(singleLogEntry);
    }

    private void initLogIndex(File parentFolder) throws IOException{
        File indexFile = new File(parentFolder.toString() + "/limits.csv");
        Writer writerFile = new FileWriter(indexFile , true);
        writer = new CsvWriter(logHeaderOrder, writerFile);
        if (!indexFile.exists()) {
            CsvWriter writer = new CsvWriter(logHeaderOrder, writerFile);
        }
    }

    private String[] processWholeLogBody(String logBody, String fileName){
        if(logBody.contains(START_MARK)){
            String[] respChunks = breakupToChunks(logBody);
            String[] respBodyList = new String[respChunks.length];
            if(respChunks != null && respChunks.length > 0){

                for(int x=0; x < respChunks.length; x++){
                    String respItem = respChunks[x];
                    String respReduced = null;

                    String callerName = getEntityNameFromLogBody(logBody);

                    // get caller name
                    String[] callSplits = null;
                    String errorString = null;

                    // process for error
                    if(respItem.contains(ERROR_MARK)){
                        errorString = respItem.substring(respItem.indexOf(ERROR_MARK) + 1);
                        errorString = errorString.substring(0, respItem.lastIndexOf(ERROR_MARK));
                        errorString = errorString.replaceAll("\n", CUSTOM_RETURN);
                        errorString = errorString.substring(0, respItem.lastIndexOf(CUSTOM_RETURN));
                    }

                    // get limits debug lines
                    respReduced = respItem.substring(respItem.indexOf(START_MARK) + 1);
                    respReduced = respReduced.trim();
                    if(errorString != null){
                        respReduced = ERROR_MARK + SEPARATOR + errorString + NEWLINE + respReduced;
                    }
                    respReduced = ENTITY_NAME + SEPARATOR + callerName + NEWLINE + respReduced;
                    respReduced = "Log File Name" + SEPARATOR + fileName + NEWLINE + respReduced;
                    respBodyList[x] = respReduced;
                    System.out.println("resp ::: " + respReduced);
                }
                return respBodyList;
            }
        }
        System.out.println("No limits found.");
        if (logBody.contains(EXCEPTION_MARK)){
            String callerName = getEntityNameFromLogBody(logBody);
            String[] respList = new String[1];
            respList[0] = ERROR_MARK + SEPARATOR + logBody.split(EXCEPTION_MARK)[1];
            respList[0] = ENTITY_NAME + SEPARATOR + callerName + NEWLINE + respList[0];
            respList[0] = "Log File Name" + SEPARATOR + fileName + NEWLINE + respList[0];
            return respList;
        }
        return new String[0];
    }

    private String getEntityNameFromLogBody(String logBody){
        String callerName = "";
        String entityLine ="";
        if(logBody.contains(ENTITY_MARKER)){
            String[] logChunks = logBody.split(ENTITY_MARKER, 3);
            if(logChunks.length == 3){
                entityLine = logChunks[2].split(NEWLINE, 2)[0];

            }
        }else if(logBody.contains("CODE_UNIT_FINISHED")){
            String[] logChunks = logBody.split("CODE_UNIT_FINISHED");
            entityLine = logChunks[logChunks.length -1].split(NEWLINE,2)[0];
        }
        callerName = entityLine.substring(entityLine.lastIndexOf("|") + 1);
        return callerName;
    }

    public String[] breakupToChunks(String resp){
        String[] nonNullSplits = null;
        try {
            String[] popSplits = resp.split(START_MARK);
            if (popSplits.length == 1) {
                // no limit chunks to process
                return null;
            }

            int j = 0;
            int nonNullCount = 0;
            while (j <= (popSplits.length - 1)) {
                if (popSplits[j].contains(END_MARK) && popSplits[j].contains("out of")) {
                    popSplits[j] = popSplits[j].split(END_MARK)[0];
                    nonNullCount++;
                } else {
                    popSplits[j] = null;
                }
                j++;
            }

            nonNullSplits = new String[nonNullCount];
            int splitsIndex = 0;

            for (int x = 0; x < popSplits.length; x++) {
                if (popSplits[x] != null && popSplits[x] != "") {
                    nonNullSplits[splitsIndex] = popSplits[x];
                    System.out.println("split :: " + nonNullSplits[splitsIndex]);
                    splitsIndex++;
                }
            }
        } catch(Exception e){
            System.out.println(e.getMessage().toString());
            System.out.println(e.getStackTrace().toString());
        }

        return nonNullSplits;
    }

}
