package salesforce;

import com.sforce.async.CSVReader;
import com.sforce.bulk.CsvWriter;
import salesforce.model.ApexLog;

import java.io.*;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Ehsan Mahmoudi on 26/02/2016.
 */
public class LogCapture {
    private static final String[] INDEX_HEADERS = new String[]{
            "Id" ,
            "STARTTIME" ,
            "STATUS",
            "APPLICATION",
            "DURATION" ,
            "LOCATION" ,
            "REQUEST" ,
            "USERID"
    };
    private static final int BUFFER_SIZE = 2048;
    ToolingAPIConnector toolingAPIConnector;
    String dataFolder ;
    public LogCapture(ToolingAPIConnector toolingAPIConnector, String dataFolder) {
        this.toolingAPIConnector = toolingAPIConnector;
        this.dataFolder = dataFolder ;
    }

    public void startTracing(String userId , String logLevelName) throws IOException {
        String llId = toolingAPIConnector.queryDebugLevel(logLevelName) ;
        toolingAPIConnector.createSingleTraceFlag(userId, llId);
    }
    public void captureLogs( double durationInSeconds  ) throws IOException {
        File indexFile = new File(dataFolder + "/index.csv");
        Set<String> storedLogs = new HashSet<>();
        if (indexFile.exists()) {
            Reader file = new FileReader(indexFile) ;
            CSVReader reader = new CSVReader(file);
            List<String> line = reader.nextRecord() ; //Reading header
            line = reader.nextRecord() ;
            while(line != null) {

                storedLogs.add(line.get(0)) ;
                line = reader.nextRecord() ;
            }
            file.close();
        }
        Writer writerFile = new FileWriter(indexFile , true);
        CsvWriter writer = new CsvWriter(INDEX_HEADERS , writerFile);

        long tic = System.currentTimeMillis() ;
        long toc = System.currentTimeMillis() ;

        while(toc - tic < durationInSeconds * 1000) {
            ApexLog [] logs = toolingAPIConnector.queryApexLogIds(new Date(tic), null) ;
            for (ApexLog log : logs) {
                writer.writeRecord(new String[]{
                        log.getId() ,
                        log.getStartTime() ,
                        log.getStatus() ,
                        log.getApplication() ,
                        log.getDurationMilliseconds() ,
                        log.getLocation() ,
                        log.getRequest() ,
                        log.getLogUserId()
                });

                writeToFile(toolingAPIConnector.querySingleApexLog(log.getId()), dataFolder + "/" +  log.getId() + ".log")  ;
            }
            try {
                Thread.sleep(20 * 1000);
            } catch (InterruptedException e) {}

            toc = System.currentTimeMillis() ;
        }
        writerFile.close();


    }

    private void writeToFile(InputStream inputStream, String fileName) throws IOException {
        byte [] buffer = new byte[BUFFER_SIZE] ;
        OutputStream out = new FileOutputStream(fileName);
        int len = inputStream.read(buffer) ;
        while (len > -1) {
            out.write(buffer , 0 , len);
            len = inputStream.read(buffer) ;
        }
        out.close();
        inputStream.close();
    }


}
