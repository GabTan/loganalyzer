package salesforce.log;

import salesforce.log.event.AggregateMap;

import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ehsan Mahmoudi on 24/03/2016.
 */
public class PipelineLineAnalyzer extends AbstractLineAnalyser {
    List<AbstractLineAnalyser>  analyzers = new ArrayList<>();

    public void addAnalyzer(AbstractLineAnalyser analyzer) {
        analyzers.add(analyzer) ;
    }


    @Override
    public void processNextLine(String line) {
        for(AbstractLineAnalyser analyzer : analyzers) {
            analyzer.processNextLine(line);
        }
    }

    @Override
    public void reset() {
        for(AbstractLineAnalyser analyzer : analyzers) {
            analyzer.reset();
        }
    }

    @Override
    public AggregateMap getResult() {
        AggregateMap map = new AggregateMap();
        for(AbstractLineAnalyser analyzer : analyzers) {
            map.aggregate(analyzer.getResult());
        }
        return map ;
    }
}
