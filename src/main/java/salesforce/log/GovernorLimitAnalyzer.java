package salesforce.log;

import salesforce.log.event.AggregateMap;
import salesforce.log.event.GovernorLimitData;
import salesforce.log.event.ListAggregator;

import java.io.Reader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ehsan Mahmoudi on 21/03/2016.
 */
public class GovernorLimitAnalyzer extends AbstractLineAnalyser {
    ListAggregator<GovernorLimitData> result ;
    GovernorLimitData data;
    boolean inDataArea ;
    Pattern SOQL = Pattern.compile(".*Number of SOQL queries: (\\d+).*") ;
    Pattern SOQL_ROWS = Pattern.compile(".*Number of query rows: (\\d+).*") ;
    Pattern DML = Pattern.compile(".*Number of DML statements: (\\d+).*") ;
    Pattern DML_ROWS = Pattern.compile(".*Number of DML rows: (\\d+).*") ;
    Pattern CPU = Pattern.compile(".*Maximum CPU time: (\\d+).*") ;
    Pattern HEAP = Pattern.compile(".*Maximum heap size: (\\d+).*") ;
    Pattern CALLOUT = Pattern.compile(".*Number of callouts: (\\d+).*") ;
    Pattern FUTURE = Pattern.compile(".*Number of future calls: (\\d+).*") ;
    Pattern QUEUEABLE = Pattern.compile(".*Number of queueable jobs added to the queue: (\\d+).*") ;
    public GovernorLimitAnalyzer() {
        reset();
    }

    @Override
    public void processNextLine(String line) {
        if (line.contains("LIMIT_USAGE_FOR_NS|(default)|")) {
            inDataArea = true ;
        }else if(line.contains("|")){
            inDataArea = false ;
        }else if(inDataArea) {
            Matcher mSOQL = SOQL.matcher(line) ;
            Matcher mSOQL_ROWS = SOQL_ROWS.matcher(line) ;
            Matcher mDML = DML.matcher(line) ;
            Matcher mDML_ROWS = DML_ROWS.matcher(line) ;
            Matcher mCPU = CPU.matcher(line) ;
            Matcher mHEAP = HEAP.matcher(line) ;
            Matcher mCALLOUT = CALLOUT.matcher(line) ;
            Matcher mFUTURE = FUTURE.matcher(line) ;
            Matcher mQUEUEABLE = QUEUEABLE.matcher(line) ;

            if (mSOQL.matches()){

                data.setSoql(Integer.valueOf(mSOQL.group(1)));
            }
            if (mSOQL_ROWS.matches()){
                data.setQueryRows(Integer.valueOf(mSOQL_ROWS.group(1)));
            }
            if (mDML.matches()){
                data.setDml(Integer.valueOf(mDML.group(1)));
            }
            if (mDML_ROWS.matches()){
                data.setDmlRows(Integer.valueOf(mDML_ROWS.group(1)));
            }
            if (mCPU.matches()){
                data.setCpu(Integer.valueOf(mCPU.group(1)));
            }
            if (mHEAP.matches()){
                data.setHeapSize(Integer.valueOf(mHEAP.group(1)));
            }
            if (mCALLOUT.matches()){
                data.setCallout(Integer.valueOf(mCALLOUT.group(1)));
            }
            if (mFUTURE.matches()){
                data.setFuture(Integer.valueOf(mFUTURE.group(1)));
            }
            if (mQUEUEABLE.matches()){
                data.setQueueble(Integer.valueOf(mQUEUEABLE.group(1)));
            }
        }


    }

    @Override
    public void reset() {
        data = new GovernorLimitData();
        result = new ListAggregator<>(data);
    }


    @Override
    public AggregateMap getResult() {
        AggregateMap m = new AggregateMap();
        m.addEvent("GOVERNOR LIMIT" , result);
        return m;
    }
}
