package salesforce.log;

import salesforce.log.event.AggregateMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

/**
 * Created by Ehsan Mahmoudi on 23/03/2016.
 */
public abstract class AbstractLineAnalyser extends AbstractSerialLogAnalyzer {
    @Override
    protected AggregateMap analyze(Reader r) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(r);
        String line = bufferedReader.readLine() ;

        while (line != null) {
            processNextLine(line) ;
            line = bufferedReader.readLine() ;
        }

        AggregateMap ret = getResult() ;
        reset() ;
        return ret ;
    }

    public abstract void processNextLine(String line) ;
    public  abstract  void reset() ;
    public abstract AggregateMap getResult() ;

}
