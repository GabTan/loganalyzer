package salesforce.log;

/**
 * Created by Ehsan Mahmoudi on 21/03/2016.
 */
public abstract class Aggregatable {

    public abstract void aggregate(Aggregatable newEvent) ;
}
