package salesforce.log.event;

import com.fasterxml.jackson.annotation.JsonProperty;
import salesforce.log.Aggregatable;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by Ehsan Mahmoudi on 21/03/2016.
 */
public class AggregateMap extends Aggregatable {
    Map<String , Aggregatable> results = new HashMap<>();

    public Set<String> getResultTypes() {
        return results.keySet() ;
    }


    public void addEvent(String type , Aggregatable event) {
        if (results.containsKey(type)) {
            results.get(type).aggregate(event);
        } else {
            results.put(type , event) ;
        }
    }

    public Aggregatable getEvent(String type) {
        return results.get(type) ;
    }

    @Override
    public void aggregate(Aggregatable newEvent) {
        AggregateMap r = (AggregateMap)newEvent ;
        for (String s : r.getResultTypes())
            addEvent(s , r.getEvent(s));
    }

    public Map<String, Aggregatable> getResults() {
        return  results ;
    }

}
