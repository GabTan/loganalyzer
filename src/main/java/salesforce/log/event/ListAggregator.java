package salesforce.log.event;

import salesforce.log.Aggregatable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Ehsan Mahmoudi on 23/03/2016.
 */
public class ListAggregator<T> extends Aggregatable{
    List<T> list = new ArrayList<>() ;

    @Override
    public void aggregate(Aggregatable newEvent) {
        ListAggregator<T> x = (ListAggregator<T>)newEvent ;
        list.addAll(x.list) ;
    }

    public List<T> getList() {
        return list;
    }

    public ListAggregator(T... l) {
        for(T e : l){
            list.add(e);
        }
    }
}
