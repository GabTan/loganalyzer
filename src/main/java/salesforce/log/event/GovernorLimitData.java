package salesforce.log.event;

import salesforce.log.Aggregatable;

/**
 * Created by Ehsan Mahmoudi on 23/03/2016.
 */
public class GovernorLimitData {

    Integer soql = 0;
    Integer queryRows = 0 ;
    Integer dml = 0 ;
    Integer dmlRows = 0 ;
    Integer cpu = 0 ;
    Integer heapSize = 0;
    Integer callout = 0 ;
    Integer future = 0 ;
    Integer queueble = 0 ;

    public Integer getSoql() {
        return soql;
    }

    public void setSoql(Integer soql) {
        this.soql = soql;
    }

    public Integer getQueryRows() {
        return queryRows;
    }

    public void setQueryRows(Integer queryRows) {
        this.queryRows = queryRows;
    }

    public Integer getDml() {
        return dml;
    }

    public void setDml(Integer dml) {
        this.dml = dml;
    }

    public Integer getDmlRows() {
        return dmlRows;
    }

    public void setDmlRows(Integer dmlRows) {
        this.dmlRows = dmlRows;
    }

    public Integer getCpu() {
        return cpu;
    }

    public void setCpu(Integer cpu) {
        this.cpu = cpu;
    }

    public Integer getHeapSize() {
        return heapSize;
    }

    public void setHeapSize(Integer heapSize) {
        this.heapSize = heapSize;
    }

    public Integer getCallout() {
        return callout;
    }

    public void setCallout(Integer callout) {
        this.callout = callout;
    }

    public Integer getFuture() {
        return future;
    }

    public void setFuture(Integer future) {
        this.future = future;
    }

    public Integer getQueueble() {
        return queueble;
    }

    public void setQueueble(Integer queueble) {
        this.queueble = queueble;
    }

}
