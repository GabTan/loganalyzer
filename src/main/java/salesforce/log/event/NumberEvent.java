package salesforce.log.event;

import salesforce.log.Aggregatable;

/**
 * Created by Ehsan Mahmoudi on 22/03/2016.
 */
public class NumberEvent extends Aggregatable {
    Integer count = 0 ;
    Integer sum = 0 ;
    Integer max  = Integer.MIN_VALUE;
    Integer min = Integer.MAX_VALUE;
    @Override
    public void aggregate(Aggregatable newEvent) {
        NumberEvent e = (NumberEvent) newEvent;
        count += e.getCount() ;
        sum += e.getSum();
        if (min > e.getMin())
            min = e.getMin() ;
        if(max < e.getMax())
            max = e.getMax() ;
    }

    public NumberEvent(Integer value) {
        count = 1 ;
        sum = value ;
        max = value ;
        min = value ;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }
}
