package salesforce.log.event;

import salesforce.log.Aggregatable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ehsan Mahmoudi on 22/03/2016.
 */
public class SoqlEvent extends Aggregatable {



    Map<String,Integer> soqlsCount = new HashMap<>() ;
    @Override
    public void aggregate(Aggregatable newEvent) {
        SoqlEvent e = (SoqlEvent) newEvent;

        for(String s : e.soqlsCount.keySet()){
            if (soqlsCount.containsKey(s))
                soqlsCount.put(s , soqlsCount.get(s) + e.soqlsCount.get(s)) ;
            else
                soqlsCount.put(s , e.soqlsCount.get(s));
        }
    }

    public SoqlEvent(Integer line , String soql) {
        soqlsCount.put("[" + line + "]" + soql , 1) ;
    }

    public Map<String, Integer> getSoqlsCount() {
        return soqlsCount;
    }


}
