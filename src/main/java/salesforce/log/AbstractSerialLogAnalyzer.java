package salesforce.log;

import salesforce.log.event.AggregateMap;

import java.io.IOException;
import java.io.Reader;

/**
 * Created by Ehsan Mahmoudi on 21/03/2016.
 */
public abstract class AbstractSerialLogAnalyzer extends AbstractBatchLogAnalyzer{
    @Override
    public AggregateMap analyze(Reader[] logContents) throws IOException {
        AggregateMap ret = new AggregateMap();
        for(Reader r : logContents) {
            AggregateMap l = analyze(r) ;
            ret.aggregate(l);
        }
        return ret ;
    }

    protected abstract AggregateMap analyze(Reader r) throws IOException;
}


