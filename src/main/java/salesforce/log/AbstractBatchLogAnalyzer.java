package salesforce.log;

import salesforce.log.event.AggregateMap;

import java.io.IOException;
import java.io.Reader;

/**
 * Created by Ehsan Mahmoudi on 21/03/2016.
 */
public abstract class AbstractBatchLogAnalyzer {
    public abstract AggregateMap analyze(Reader[] logContents) throws IOException;
}
