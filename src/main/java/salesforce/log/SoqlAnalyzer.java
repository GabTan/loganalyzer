package salesforce.log;

import salesforce.log.event.AggregateMap;
import salesforce.log.event.SoqlEvent;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ehsan Mahmoudi on 24/03/2016.
 */
public class SoqlAnalyzer extends AbstractLineAnalyser {
    AggregateMap result ;
    Pattern QUERY = Pattern.compile(".*\\|SOQL_EXECUTE_BEGIN\\|\\[(\\d+)\\]\\|Aggregations:\\d*\\|(.*)") ;

    public SoqlAnalyzer() {
        reset();
    }

    @Override
    public void processNextLine(String line) {
        Matcher m = QUERY.matcher(line) ;
        if(m.matches()) {
            result.addEvent("SOQL QUERY" , new SoqlEvent(Integer.valueOf(m.group(1)) , m.group(2)));
        }
    }

    @Override
    public void reset() {
        result = new AggregateMap();
    }

    @Override
    public AggregateMap getResult() {
        return result ;
    }
}
