package salesforce.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Ehsan Mahmoudi on 26/02/2016.
 */
@JsonIgnoreProperties({"attributes"})
public class ApexLog {
    String application ;
    String durationMilliseconds ;
    String location ;
    String logLength ;
    String logUserId ;
    String operation ;
    String startTime ;
    String request ;
    String status ;
    String Id;

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getDurationMilliseconds() {
        return durationMilliseconds;
    }

    public void setDurationMilliseconds(String durationMilliseconds) {
        this.durationMilliseconds = durationMilliseconds;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLogLength() {
        return logLength;
    }

    public void setLogLength(String logLength) {
        this.logLength = logLength;
    }

    public String getLogUserId() {
        return logUserId;
    }

    public void setLogUserId(String logUserId) {
        this.logUserId = logUserId;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        this.Id = id;
    }
}
