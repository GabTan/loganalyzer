package salesforce.model;

/**
 * Created by Ehsan Mahmoudi on 25/02/2016.
 */
public class TraceFlag {
    String debugLevelId ;
    String logType ;
    String traceEntityId ;

    public String getDebugLevelId() {
        return debugLevelId;
    }

    public void setDebugLevelId(String debugLevelId) {
        this.debugLevelId = debugLevelId;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public String getTraceEntityId() {
        return traceEntityId;
    }

    public void setTraceEntityId(String traceEntityId) {
        this.traceEntityId = traceEntityId;
    }
}
