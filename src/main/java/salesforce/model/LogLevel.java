package salesforce.model;

/**
 * Created by Ehsan Mahmoudi on 26/02/2016.
 */
public enum LogLevel {
    NONE, ERROR, WARN, INFO , DEBUG , FINE , FINER , FINEST ;
}
