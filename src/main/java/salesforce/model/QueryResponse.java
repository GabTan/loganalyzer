package salesforce.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Ehsan Mahmoudi on 26/02/2016.
 */
public class QueryResponse<T> {
    int size ;
    int totalSize ;
    boolean done ;
    String queryLocator ;
    String entityTypeName ;

    T[] records;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(int totalSize) {
        this.totalSize = totalSize;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getQueryLocator() {
        return queryLocator;
    }

    public void setQueryLocator(String queryLocator) {
        this.queryLocator = queryLocator;
    }

    public T[] getRecords() {
        return records;
    }

    public void setRecords(T[] records) {
        this.records = records;
    }

    public String getEntityTypeName() {
        return entityTypeName;
    }

    public void setEntityTypeName(String entityTypeName) {
        this.entityTypeName = entityTypeName;
    }
}
