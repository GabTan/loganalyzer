package salesforce.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Ehsan Mahmoudi on 26/02/2016.
 */

public class DebugLevel {
    LogLevel apexCode ;
    LogLevel apexProfiling ;
    LogLevel Callout ;
    LogLevel database ;
    LogLevel developerName ;
    String Id ;

    public LogLevel getApexCode() {
        return apexCode;
    }

    public void setApexCode(LogLevel apexCode) {
        this.apexCode = apexCode;
    }

    public LogLevel getApexProfiling() {
        return apexProfiling;
    }

    public void setApexProfiling(LogLevel apexProfiling) {
        this.apexProfiling = apexProfiling;
    }

    public LogLevel getCallout() {
        return Callout;
    }

    public void setCallout(LogLevel callout) {
        Callout = callout;
    }

    public LogLevel getDatabase() {
        return database;
    }

    public void setDatabase(LogLevel database) {
        this.database = database;
    }

    public LogLevel getDeveloperName() {
        return developerName;
    }

    public void setDeveloperName(LogLevel developerName) {
        this.developerName = developerName;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        this.Id = id;
    }
}
