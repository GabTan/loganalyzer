package salesforce;

import java.util.ArrayList;
import java.util.List;

import com.sforce.async.AsyncApiException;
import com.sforce.async.BulkConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.sforce.soap.apex.Connector;
import com.sforce.soap.apex.SoapConnection;
import com.sforce.soap.partner.DescribeSObjectResult;
import com.sforce.soap.partner.PartnerConnection;
import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;
import com.sforce.ws.ConnectorConfig;
import salesforce.configuration.ConnectionConfig;

public class SalesforceDataConnector {

	protected static final Logger logger = LoggerFactory.getLogger(SalesforceDataConnector.class);
    private ConnectorConfig connectorConfig;
    private PartnerConnection partnerConnection ;
    private SoapConnection apexConnection ;
    private ConnectionConfig connectionConfig ;
    private BulkConnection bulkConnection ;
    public SalesforceDataConnector(ConnectionConfig connectionConfig) {
        this.connectionConfig = connectionConfig;
    }

    /**
     * Creates a connection. If already a connection exist, it will return the old one.
     * @return Enterprise connection that is created. If connection is created it will return the existing one.
     * @throws com.sforce.ws.ConnectionException
     */
	public PartnerConnection getConnection() throws ConnectionException {
        if (partnerConnection != null)
            return partnerConnection ;
		connectorConfig = new ConnectorConfig();
        String serverUrl = connectionConfig.getServerUrl() ;
        if (serverUrl.toLowerCase().endsWith(".com/") ) {
            serverUrl += "services/Soap/u/33.0" ;
        } else if( serverUrl.toLowerCase().endsWith(".com") ){
            serverUrl += "/services/Soap/u/33.0" ;

        }

        connectorConfig.setAuthEndpoint(serverUrl);
        if (connectionConfig.getProxyHost() != null && connectionConfig.getProxyHost().trim().length()  > 0)
        connectorConfig.setProxy(connectionConfig.getProxyHost(),
                connectionConfig.getProxyPort());
		connectorConfig.setUsername(connectionConfig.getUsername());
		connectorConfig.setPassword(connectionConfig.getPassword() +
                connectionConfig.getToken());
		partnerConnection = new PartnerConnection(connectorConfig);
        return partnerConnection ;
	}

    public BulkConnection getBulkConnection() throws ConnectionException, AsyncApiException {
        if(bulkConnection != null)
            return bulkConnection ;
        getConnection() ;
        ConnectorConfig bulkConfig = new ConnectorConfig() ;
        if (connectionConfig.getProxyHost() != null && connectionConfig.getProxyHost().trim().length()  > 0)
            bulkConfig.setProxy(connectionConfig.getProxyHost(),
                    connectionConfig.getProxyPort());
        bulkConfig.setUsername(connectionConfig.getUsername());
        bulkConfig.setPassword(connectionConfig.getPassword() +
                connectionConfig.getToken());
        bulkConfig.setSessionId(connectorConfig.getSessionId());
        bulkConfig.setRestEndpoint(connectorConfig.getServiceEndpoint().substring(0,
                connectorConfig.getServiceEndpoint().indexOf("/Soap/u/")) + "/async/34.0");
        bulkConnection = new BulkConnection(bulkConfig) ;
        return bulkConnection ;
    }
    public SoapConnection getSoapConnection() throws ConnectionException {
        if (apexConnection != null)
            return apexConnection ;
        getConnection() ;
        ConnectorConfig soapConfig = new ConnectorConfig() ;
        if (connectionConfig.getProxyHost() != null && connectionConfig.getProxyHost().trim().length()  > 0)
            soapConfig.setProxy(connectionConfig.getProxyHost(),
                connectionConfig.getProxyPort());
        soapConfig.setUsername(connectionConfig.getUsername());
        soapConfig.setPassword(connectionConfig.getPassword() +
                connectionConfig.getToken());
        soapConfig.setSessionId(connectorConfig.getSessionId());
        soapConfig.setServiceEndpoint(connectorConfig.getServiceEndpoint().replace("/u/", "/s/"));
        apexConnection = Connector.newConnection(soapConfig);
        return apexConnection ;
    }

    public ToolingAPIConnector getToolingAPIConnector() throws ConnectionException {
        return new ToolingAPIConnector(getSessionId() , getConnection().getConfig().getServiceEndpoint().substring(0 ,
                getConnection().getConfig().getServiceEndpoint().indexOf(".com/") + 4 )
        );
    }
    public String getSessionId() throws ConnectionException {
        return getConnection().getConfig().getSessionId() ;
    }

    /**
     * Queries salesforce for SObjects. This will download he full query result and handles the QueryResult and QueryLocator
     * behinde the scene
     * @param query The string query
     * @return List of the result
     * @throws com.sforce.ws.ConnectionException
     */
    public  List<SObject> query(String query) throws ConnectionException {
        logger.info("Running the query : " + query);
        QueryResult result = getConnection().query(query) ;
        List<SObject> ret = new ArrayList<>() ;
        for (SObject obj : result.getRecords()) {
            ret.add( obj);
        }
        int cnt = 0 ;
        while (!result.isDone()) {
            result = getConnection().queryMore(result.getQueryLocator()) ;
            for (SObject obj : result.getRecords()) {
                ret.add( obj);
            }
            if (cnt >= 10) {
                logger.info("Loaded {} records. Continue ... "  , ret.size());
                cnt = -1 ;
            }

            cnt++ ;
        }
        logger.info("Total " + ret.size() + " records loaded.");
        return ret ;
    }

    public DescribeSObjectResult getDescribe(String objectName) throws ConnectionException {
        return getConnection().describeSObject(objectName) ;
    }


}